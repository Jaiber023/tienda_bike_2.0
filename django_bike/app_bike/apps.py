from django.apps import AppConfig


class AppBikeConfig(AppConfig):
    name = 'app_bike'
