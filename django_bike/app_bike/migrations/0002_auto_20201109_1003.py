# Generated by Django 3.1.2 on 2020-11-09 13:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_bike', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='bicicleta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num_serie', models.CharField(max_length=10, unique=True)),
                ('modelo', models.CharField(max_length=50)),
                ('anio', models.IntegerField()),
                ('marca', models.CharField(max_length=10)),
                ('imagen', models.ImageField(null=True, upload_to='bicicletas')),
            ],
        ),
        migrations.RenameModel(
            old_name='Usuario',
            new_name='Usuariox',
        ),
    ]
