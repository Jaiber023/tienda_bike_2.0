from django.db import models

# Create your models here.
class Usuariox(models.Model):
    id_user = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    telefono = models.CharField(max_length=10)
    correo = models.EmailField()
    class Meta:
        db_table = "usuario"


# campos para subir la imagenes de la bicicleta
class bicicleta(models.Model):
    num_serie = models.CharField(max_length=10,unique=True)
    modelo = models.CharField(max_length=50)
    anio = models.IntegerField()
    marca = models.CharField(max_length=10)
    imagen = models.ImageField(upload_to="bicicletas", null=True)
    def __srt__(self):
        return self.num_serie