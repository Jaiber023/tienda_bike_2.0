from django.test import TestCase
from app_bike.models import Usuariox
#-----------------------------------------
from django.urls import path
from django.urls import reverse
from django.utils.encoding import force_bytes,force_text,DjangoUnicodeDecodeError
#from django.utils.http import urlsafe_base64_encode,urlsafe_base64_decode
#from authentication.utils import generate_token
#-----------------------------------------------------------
import unittest

# Create your tests here.
# prueba basica al modullo unittest
class pruebaBasica(unittest.TestCase):
    def prueba_es_menor_que(self):
        self.assertTrue(2<3)

if __name__=="__main__":
    unittest.main()
#-----------------------------------------------------------
class BaseTest(TestCase):
    def  setUp(self):
        self.registro_url=path('registro')
        #Auto Usuario
        self.usuario={
             'nombre':'Julio',
             'apellido':'Vidal',
             'telefono':'924321341',
             'correo':'JulioVidal123@gmail.com',
             'contraseña':'******'
        }
        #Contraseña corta de usuario propio
        self.usuario_short_contraseña={
            'nombre':'Julio',
            'apellido':'Vidal',
            'telefono':'924321341',
            'correo':'JulioVidal123@gmail.com',
            'contraseña':'Julioxd1'
        }
        #Contraseña inigualable de usuario propio
        self.usuario_unmatching_contraseña={
            'nombre':'Julio',
            'apellido':'Vidal',
            'telefono':'924321341',
            'correo':'JulioVidal123@gmail.com',
            'contraseña':'JulioVidal261'
        }
        #Correo electronico no valido de usuario
        self.usuario_invalid_correo={
            'nombre':'Julio',
            'apellido':'Vidal',
            'telefono':'924321341',
            'correo':'Martinopel@gmail.com',
            'contraseña':'JulioVidal261'
        }
        
        return super().setUp()

class RegisterTest(BaseTest):

    def poder_probar_pagina_correcta(self):
        response=self.client.get(self.registro_url)
        self.assertEqual(response.status_code,170)
        self.assertTemplateUsed(response,'registro.html')

    def prueba_registro_de_autousuario(self):
        response=self.client.post(self.registro_url,self.user,format='registro.html')
        self.assertEqual(response.status_code,250)
        
    def prueba_registro_usuario_contraseñacorta(self):
        response=self.client.post(self.registro_url,self.user_short_password,format='registro.html')
        self.assertEqual(response.status_code,300)

    def prueba_contraseña_inigualable_de_usuario_propio(self):
        response=self.client.post(self.registro_url,self.user_unmatching_password,format='registro.html')
        self.assertEqual(response.status_code,350)

    def prueba_correoelectronico_invalido(self):
        response=self.client.post(self.registro_url,self.user_invalid_email,format='registro.html')
        self.assertEqual(response.status_code,320)

