from django.shortcuts import render, redirect
from app_bike.forms import usuarioForms
from app_bike.forms import Usuariox
from app_bike.models import *
#imports para validacion de usuarios
# se usan los metodos de las siguientes librebrias
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
# no se arregla el error de los objetos del models usario
# clase para agregar usuarios en la base de datoos 
def usuarios_x(request):
    l_usuario = Usuariox.objects.all()
    if request.method== "POST":
        form = usuarioForms(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/usuario')
            except:
                pass
    else:
        form =usuarioForms()

    return render(request,'form.html',{'form':form,'usuario':l_usuario}) 



#Metodo para editar usando id del usuario
def editar(request,id):
    usua = Usuariox.objects.get(id_user=id)
    form = usuarioForms(instance=usua)
    return render(request,'editar.html',{'form':form,'id_user':usua.id_user})

def modificar(request,id):
    usua = Usuariox.objects.get(id_user=id)
    if request.method =="POST":
        form = usuarioForms(request.POST,instance=usua)
        if form.is_valid():
            try:
                form.save()
                
            except:
                pass
#cuando guarde los cambios vuelve a cargar los datos que estan en la lista
    l_usuario = Usuariox.objects.all()
    form =usuarioForms()
    return render(request,'form.html',{'form':form,'usuario':l_usuario})         



#metodo para por poder eliminar 
def eliminar(request,id):
    usua = Usuariox.objects.get(id_user=id)
    usua.delete()
    l_usuario = Usuariox.objects.all()
    form =usuarioForms()
    return render(request,'form.html',{'form':form,'usuario':l_usuario})         

# zona para ver si el usuario inicio sesion
def sesionPage (request):
    if request.user.is_authenticated:
        return redirect('/usuario')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request,username = user, password = password)

            if user is not None:
                login(request,user)
                return redirect('/usuario')
            else:
                messages.info(request, 'Usuario o clave incorrecto')
                #lo manda a iniciar sesion
        return render(request,'sesion.html')