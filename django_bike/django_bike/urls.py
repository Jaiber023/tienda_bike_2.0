"""django_bike URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django_bike import views
from django_bike.views import index
from django_bike.views import registro
from django_bike.views import sesion
from django_bike.views import nosotros
from app_bike import views
# Imports para caragar las imagenes
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('admin/', admin.site.urls),
    #path('hola/', views.holamundo),
    path('index/', index),
    path('registro/',registro),
    path('sesion/', sesion),
    path('nosotros/', nosotros),
    path('usuario/', views.Usuariox),
    path('editar/<int:id>',views.editar),
    path('eliminar/<int:id>',views.eliminar),

#URLS DE LOS USUARIOS
path('sesion/', views.sesionPage, name='login')

]
# Configuracion para ir a buscar la imagenes a la carpeta
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)