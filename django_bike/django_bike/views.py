from django.http import HttpResponse
from django.shortcuts import render

#prueba de pagina
def holamundo(request):
    return HttpResponse("<h1>hola django framework</h1>")

#Esto es para llamar a la pagina de saludo.html
def index(request):
    return render(request, 'inicio.html')

def registro(request):
    return render(request, 'registro.html')

def sesion(request):
    return render(request,'sesion.html')

def nosotros(request):
    return render(request,'nosotros.html')