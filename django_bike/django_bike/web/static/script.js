/** Funcionamientos de la ubicacion, con coordenadas de santiago...
 * No funciona el marcado por que no cuento con la credenciales
 */
function iniciarMapa(){
    var coordenada = {lat : -33.4372  , lng: -70.6506 };
    var map = new google.maps.Map(document.getElementById('map'),{
        zoom: 10, center: coordenada
    });
    var market = new google.maps.market({
        position: coordenada, map : map
    });
}